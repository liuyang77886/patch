package cn.chendd.patch.ui.components.table.render;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * <pre>
 * 表格的操作列渲染
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-30
 *    fix->1.
 *         2.
 * </pre>
 */
public class TableOperationButtonEditor extends DefaultCellEditor {

	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JButton resetImportButton;
	private JButton placeImportButton;
	private int row;
	private int column;
	
	public TableOperationButtonEditor(JTextField textField) {
		super(textField);
		this.setClickCountToStart(1);
		panel.setLayout(null);
		resetImportButton = new JButton("重新导入");
		resetImportButton.setBounds(0, 2, 85, 20);
		placeImportButton = new JButton("置导入");
		placeImportButton.setBounds(90, 2, 70, 20);
		resetImportButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireEditingStopped();//特殊用途
			}
		});
		panel.add(resetImportButton);
		panel.add(placeImportButton);
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table , Object value , boolean isSelected , int row , int column){
		panel.setBackground(table.getBackground());
		this.row = row;
		this.column = column;
		return panel;
	}

	/**
	 * @return Returns the row.
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return Returns the column.
	 */
	public int getColumn() {
		return column;
	}
	
}
