package cn.chendd.patch.ui.adapters.enums;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import cn.chendd.patch.ui.MainFrame;
import cn.chendd.utils.MyImageIcon;

/**
 * <pre>
 * 系统参数设置
 * </pre>
 *
 */
public class ParamSetPanel extends TabPanel {

	public ParamSetPanel(MainFrame mainFrame, JTabbedPane tab) {
		super(mainFrame, tab);
	}

	private static final long serialVersionUID = -6666114035415285248L;

	@Override
	protected void initElement() {
		
		JPanel panel = new JPanel(){
			
			private static final long serialVersionUID = 1L;
			
			@Override
			protected void paintComponent(Graphics g) {
				g.setColor(Color.RED);
				g.setFont(new Font("宋体" , Font.BOLD , 20));
				g.drawString("免责声明", 20, 30);
				g.setColor(Color.BLACK);
				g.setFont(new Font("宋体" , Font.PLAIN , 12));
				int width = mainFrame.windowWidth - 12;
				g.drawString("本程序不为最终的补丁包文件实现，请勿用于其他用途，如有带来任何问题，均与作者无关！", 42, 60);
				g.drawString("如果您打开本软件，表示您同意以上声明。", width - 300, 80);
				/*g.drawRect(0, 0, width, 400);*///------测试用
				MyImageIcon guojia = new MyImageIcon("name.png");
				Image image = guojia.getImage();
				g.drawImage(image, width - 300, 100, null);
				//---功能:使用方法:
				g.setColor(Color.RED);
				g.setFont(new Font("宋体" , Font.BOLD , 20));
				g.drawString("使用方法（请一定保持编译目录为最新代码）", 20, 150);
				g.setColor(Color.BLACK);
				g.setFont(new Font("宋体" , Font.PLAIN , 12));
				int y = 180;
				String tips[] = {
					"我们约定列表文件中的路径为SVN中工程路径后的部分，以/开头；编译目录则以部署后如Tomcat中webapps目录下的应用目录，如ROOT下；",
					"点击选择列表文件处的“浏览...”按钮，或者在文本框中手动填写列表文件的路径，可支持（*.txt，*.csv，*.xls，*.xlsx）；",
					"点击“载入文件”按钮，系统将展示出选择文件中的具体明细数据；",
					"点击选择编译目录处的“浏览...”按钮等，同上（2）；",
					"点击“生成补丁”按钮，系统将进行具体补丁文件生成操作；",
					"在（3）中的列表数据中选择一行点击右键，可将相关的文件进行“忽略”和“取消忽略”的操作，被忽略的数据项不参与补丁文件生成；",
					"如果某项数据的路径在编译目录下不存在，则本次数据项的补丁文件将跳过生成；",
					"如果操作成功,则会在桌面生成当前年月日时分的文件夹，补丁文件放置在code目录下；"
				};
				for (int i=0 , lens = tips.length ; i < lens ; i++) {
					g.drawString("（" + (i+1) + "）、" + tips[i] , 40, y + 22 * i);
				}
				//---其它说明
				g.setColor(Color.RED);
				g.setFont(new Font("宋体" , Font.BOLD , 20));
				g.drawString("其它说明", 20, y + 22 * 9);
				g.setColor(Color.BLACK);
				g.setFont(new Font("宋体" , Font.PLAIN , 12));
				String others[] = {
					"菜单【设置】--【参数设置】可进行相关的参数调整；",
					"点击窗口最小化可显示至系统托盘；",
					"双击（右键显示菜单）系统托盘图标可显示窗口；"
				};
				for (int i = 0; i < others.length; i++) {
					g.drawString("（" + (i+1) + "）、" + others[i] , 40, y + 22 * (9 + i + 1));
				}
			}
		};
		panel.setLayout(null);
		panel.setBounds(0, 0, super.mainFrame.windowWidth, mainFrame.windowHeight);
		panel.setBackground(Color.PINK);
		
		this.add(panel);
	}

}
