package cn.chendd.patch.ui.adapters.enums;


/**
 * <pre>
 * 主页tab页面面板枚举定义
 * </pre>
 */
public enum EnumTabPanel {

	FILE_LIST_TAB("文件列表" , FileListPanel.class),
	PARAM_SET_TAB("软件描述" , ParamSetPanel.class),
	PARAM_RUN_LOG("运行日志" , RunLogPanel.class),
	;
	
	private String text;
	private Class<?> clazz;
	
	private EnumTabPanel(String text , Class<?> clazz){
		this.text = text;
		this.clazz = clazz;
	}

	/**
	 * @return Returns the text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return Returns the clazz.
	 */
	public Class<?> getClazz() {
		return clazz;
	}

}
