/**
 * 
 */
package cn.chendd.patch.ui.components.table;

import java.util.LinkedHashMap;
import java.util.List;

import cn.chendd.patch.vo.DataVo;

/** 
 * <pre>
 * 空的dataTable实现
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-20
 *    fix->1.
 *         2.
 * </pre> 
 */
public class SimpleDataTable extends DataTable {

	public static final long serialVersionUID = 1L;
	
	@Override
	public String[] getHeaders() {
		return null;
	}

	@Override
	public List<? extends DataVo> getDataVoList() {
		return null;
	}

	@Override
	public List<LinkedHashMap<String, Object>> getDataMapList() {
		return null;
	}
	
	@Override
	public Integer[] getColumnWidth() {
		return null;
	}

}
