package cn.chendd.utils;

import javax.swing.ImageIcon;

public class MyImageIcon extends ImageIcon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyImageIcon(String path){
		super(MyImageIcon.class.getResource("/cn/chendd/patch/images/" + path));
	}
	
}
