package cn.chendd.patch.ui.adapters.impls;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import cn.chendd.patch.enums.EnumProperties;
import cn.chendd.utils.PropertiesUtil;



public abstract class PathAdapter {

	protected String compileFolder;
	protected String regex;
	protected File dstFolder;
	protected String path;
	
	public PathAdapter(String compileFolder , String regex , File dstFolder , String path){
		this.compileFolder = compileFolder;
		this.regex = regex;
		this.dstFolder = dstFolder;
		this.path = path;
		reloadPath();
	}
	
	public abstract void patchByPath() throws IOException;

	protected void reloadPath() {
		//根据配置项替换源路径中的参数
		Properties props = new PropertiesUtil(EnumProperties.REPLACE_PROPERTIES).getProperties();//每次都会读取文件
		Set<Entry<Object , Object>> entrySet = props.entrySet();
		for (Entry<Object, Object> entry : entrySet) {
			String key = (String) entry.getKey();
			String replace = "_replace";
			if(StringUtils.endsWith(key, replace)) {
				continue;
			}
			String value = (String) entry.getValue();
			String replaceValue = props.getProperty(key + replace);
			this.path = StringUtils.replace(path, value, replaceValue);
		}
	};
	
	public abstract boolean patchFilterRule();
	
}
