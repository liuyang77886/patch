package cn.chendd.patch.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import cn.chendd.utils.MyImageIcon;

/**
 * 
 * <pre>
 * 启动页面
 * </pre>
 */
public class LaunchWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private Image splashImage = new MyImageIcon("splash.png").getImage();
	public JProgressBar bar = new JProgressBar(0, 100);
	public JLabel textLabel = new JLabel();
	
	public LaunchWindow(){
		super.setTitle("正在启动中...");
		super.setIconImage(new MyImageIcon("logo.png").getImage());
		super.setSize(500 , 320);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLocationRelativeTo(null);
		super.setAlwaysOnTop(true);
		super.setUndecorated(true);
		JPanel textPanel = new JPanel(){
			private static final long serialVersionUID = 1L;
			@Override
			protected void paintComponent(Graphics g) {
				g.drawImage(splashImage, 0, 0, null);
			}
		};
		textPanel.setLayout(null);
		textLabel.setFont(new Font("宋体" , Font.PLAIN , 12));
		textLabel.setBounds(5, 230, 500, 20);
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		//设置文本显示位置
		textLabel.setText("载入工作台");
		textPanel.add(textLabel);
		bar.setBorder(null);
		bar.setForeground(new Color(10,36,106));
		bar.setBackground(new Color(212,208,200));
		southPanel.add(bar);
		this.add(textPanel);
		this.add(southPanel, BorderLayout.SOUTH);
		super.setVisible(true);
	}
	
	public void setProressBarValue(int value , String text){
		this.bar.setValue(value);
		this.textLabel.setText(text);
	}
	
	/**
	 * <pre>
	 * 进度条结束，关闭进度窗口
	 * </pre>
	 */
	public void endProgressBar(String[] args){
		MainWindow.launch(MainWindow.class, args);
		this.setVisible(false);
	}
	
}
