package cn.chendd.patch.ui.adapters.enums;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.lang.StringUtils;

import cn.chendd.patch.enums.EnumProperties;
import cn.chendd.utils.MyImageIcon;
import cn.chendd.utils.PropertiesUtil;

public class ReplaceParamPanel extends SetPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReplaceParamPanel(SetFrameAdapter setFrame) {
		super(setFrame);
	}


	@Override
	protected void initElement() {
		JPanel paramSmbPanel = this;
		paramSmbPanel.setLayout(null);
		paramSmbPanel.setBounds(10, 10, super.width, super.height);
		Properties props = new PropertiesUtil(EnumProperties.REPLACE_PROPERTIES).getProperties();
		Set<Object> entrySet = props.keySet();
		TreeSet<String> treeSet = new TreeSet<String>();
		//将props按顺序读取
		for (Object key : entrySet) {
			treeSet.add(key.toString());
		}
		Iterator<String> it = treeSet.iterator();
		int index = 0;
		while(it.hasNext()) {
			String key = it.next();
			int count = index/2 + 1;
			String name = "源参数" + count + "：";
			if(StringUtils.endsWith(key, "_replace")) {
				name = count + "替换为：";
			}
			JLabel nameLabel = new JLabel(name);
			nameLabel.setFont(font);
			nameLabel.setBounds(10, 10 + index * 32, 100, 30);
			// 加文本框
			JTextField nameText = new JTextField();
			nameText.setBounds(105, 12 + index * 32, 220, 26);
			nameText.setText(props.getProperty(key));
			nameText.setName(key);
			// 加入panel
			paramSmbPanel.add(nameLabel);
			paramSmbPanel.add(nameText);
			index++;
		}
		
		JPanel buttonPanel = new JPanel();
		JButton saveButton = new JButton("保存");
		saveButton.setIcon(new MyImageIcon("save.gif"));
		saveButton.addMouseListener(new SaveParamPropsAdapter(this , EnumProperties.REPLACE_PROPERTIES));
		buttonPanel.add(saveButton);
		buttonPanel.setBounds(17, 50 + (props.size()-1) * 30, super.height - 55, 50);
		paramSmbPanel.add(buttonPanel);
	}

}
