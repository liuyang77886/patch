package cn.chendd.patch.ui.adapters.enums;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import cn.chendd.patch.ui.MainFrame;

/** 
 * <pre>
 * 面板超类
 * </pre>
 *
 * <pre>
 * modify by chendd on 2017-6-28
 *    fix->1.
 *         2.
 * </pre> 
 */
public abstract class TabPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	protected MainFrame mainFrame;
	protected JTabbedPane tab;
	
	public TabPanel(MainFrame mainFrame , JTabbedPane tab){
		this.mainFrame = mainFrame;
		this.tab = tab;
	}
	
	public void init(){
		this.setLayout(null);
		initElement();
	}
	
	protected abstract void initElement();
	
}
