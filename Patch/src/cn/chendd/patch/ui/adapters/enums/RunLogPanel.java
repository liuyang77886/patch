package cn.chendd.patch.ui.adapters.enums;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import cn.chendd.patch.ui.MainFrame;

public class RunLogPanel extends TabPanel {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2877627768761308756L;

	public RunLogPanel(MainFrame mainFrame, JTabbedPane tab) {
		super(mainFrame, tab);
	}

	@Override
	protected void initElement() {
		
		JPanel panel = new JPanel(){
			
			private static final long serialVersionUID = 1L;
			
			@Override
			protected void paintComponent(Graphics g) {
				g.setColor(Color.RED);
				g.setFont(new Font("宋体" , Font.BOLD , 60));
				int center = 72;
				g.drawString("o(╯□╰)o看日志文件吧", center, 200);
			}
		};
		panel.setLayout(null);
		panel.setBounds(0, 0, super.mainFrame.windowWidth, mainFrame.windowHeight);
		panel.setBackground(Color.PINK);
		
		this.add(panel);
	}
}
