package cn.chendd.patch.enums;

/**
 * <pre>
 * 定义忽略状态
 * </pre>
 *
 * <pre>
 * modify by chendd on 2018-8-6
 *    fix->1.
 *         2.
 * </pre>
 */
public enum EnumExcept {

	EXCEPT("已忽略"),
	;
	
	private String key;
	
	private EnumExcept(String key){
		this.key = key;
	}

	public String getKey() {
		return key;
	}
	
}
